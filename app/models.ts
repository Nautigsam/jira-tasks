export type Task = {
  title: string;
  context: string;
  expectations: string;
};
