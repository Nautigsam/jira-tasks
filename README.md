# jira-tasks

## Getting started

Prerequisites:
* docker
* docker-compose

```
$ git clone https://framagit.org/Nautigsam/jira-tasks.git
$ cd jira-tasks
# Start
$ docker-compose up -d
# Stop
$ docker-compose down
```

## Dev server

Prerequisites:
* [Deno](https://deno.land/#installation) >= 1.9.0
* [Denon](https://github.com/denosaurs/denon)

```
# Start and watch modifications
$ denon start
```